# Laravel 8 Modular App Plugin

[![Laravel](https://img.shields.io/badge/laravel-8-orange.svg)](http://laravel.com)

Turn your existing laravel app into modular application. Laravel modular plugin allows you to write modular plugins for laravel.

Let's say, you are building a blog application. Your blog needs to have following features:

- comments
- blog post
- user management etc..

You can turn this features into a module and bundle your logic so that you can easily use this module for your other projects. You can easily turn on/off your module.

# Plugin Requirements

- PHP >= 8.0
- Laravel >= 8.0

# Older Laravel Plugin
For older version <= 6 of Laravel use this plugin [Laravel Modular](https://github.com/learn2torials/laravel-modular)

# How to install this plugin

Run following command to your existing project.

```

# install this plugin
composer require "learn2torials/modular-laravel"

# create module using artisan
php artisan make:module comments
```

Above command will create a new directory under App/Modules with following structure.

```
App
|- Modules
   |-- Comments
      |-- Controllers
      |-- Models
      |-- Views
      |-- Migrations
         |-- Seeder
      |-- Translations
         |-- en
             |-- general.php
         |-- fr
             |-- general.php
      |-- config.php
      |-- routes.php
```

Next, once this folder structure is generated you can turn on this module by creating _config/modular.php_ file in _config_ directory.

```
<?php

/*
|--------------------------------------------------------------------------
| Configuration File
|--------------------------------------------------------------------------
|
| You can overwrite default configuration here according to your app requirements.
|
*/
return [
    "i18n"     => false,
    "https"    => false,
    "modules"  => [
        "comments" => true
    ]
];
```

That is it, your module is now enabled. You can verify that your module is working by browsing

```
# without prefix
http://yourdomain.com/

# add prefix key in app/Modules/Comments/config.php file and then go to
http://yourdomain.com/comments
```

### Enable translation for you module. Set following config in _config/modular.php_ file.

```
"i18n" => true,
```

Now, your module url will be:

```
http://yourdomain.com/en/ca/comments      -> for english translation
http://yourdomain.com/fr/ca/comments      -> for french translation
```

### When prefix is enabled

```
http://yourdomain.com/en/ca/admin/comments -> if prefix is admin
http://yourdomain.com/fr/ca/admin/comments -> if prefix is admin
```

How to use translations. Check your view file in your module to get the idea of usage:

```
{{ __('module::file_name.translation_key') }}
```

### Module Configurations

Once module is enabled you can access module related configurations using following syntax.

For example: if you have installed a user module. Configuration file for user module is located in _Modules/User/config.php_

```
<?php

/*
|--------------------------------------------------------------------------
| User Module Configurations
|--------------------------------------------------------------------------
|
| Here you can add configurations that relates to your [User] module
| Only make sure not to add any other configs that do not relate to this
| User Module ...
|
*/
return [

    // module name
    'name' => 'User',

    // module route prefix
    "prefix" => "user",

    // register middleware
    'middleware' => [
        'user' => \App\Modules\User\Middleware\UserAuthenticated::class,
    ],

    // register service providers
    'providers' => [
        \App\Modules\User\Provider\UserProvider::class
    ],

    // register route middleware
    'route_middleware' => ['user'],

    // database seeder
    'seeder' => [
        __DIR__. '/Migrations/Seeder/UsersTableSeeder.php'
    ]
];
```

### Add prefix for your modules. Set following config.php file of your module

```
"prefix" => "admin",
```

Now, your module url will be:

```
http://yourdomain.com/admin/comments
```

### How to run module migration/seeder

To run migration or seeder for your modules. Add seeder to config file and run following commands.

```

# run module migrations
php artisan migrate

# run module seeders
php artisan db:seed --class="L2T\Modular\Database\Seeder"
```

# Reference

Example is shown on [https://learn2torials.com/a/laravel-module-management](https://learn2torials.com/a/laravel-module-management)
