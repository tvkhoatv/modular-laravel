<?php

namespace L2T\Modular\Providers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;
use L2T\Modular\Commands\ModuleCommand;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    private $config = 'modular';

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(dirname(__DIR__). '/config.php', $this->config);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->publishes([
            dirname(__DIR__). '/config.php' => config_path($this->config. '.php')
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                ModuleCommand::class
            ]);
        }

        $this->enableI18n()
             ->enableModules();

        Auth::provider('User', function($app, array $config) {
            return new UserServiceProvider($app['hash'], $config['model']);
        });
    }

    /**
     *
     * Enable module functionality
     */
    private function enableModules(): void
    {
        $moduleBasePath = $modulePath = app_path(). '/Modules/';

        foreach (config($this->config. '.modules', []) as $module => $isTurnedOn)
        {
            if ($isTurnedOn)
            {
                $modulePath = $moduleBasePath .Str::studly($module). DIRECTORY_SEPARATOR;

                $moduleName = Str::snake($module);
                $moduleConfigPath = $modulePath. 'config.php';

                if( File::exists($moduleConfigPath) ) {

                    $this->mergeConfigFrom($moduleConfigPath, $moduleName);
                    $this->loadViewsFrom($modulePath.'Views', $moduleName);
                    $this->loadMigrationsFrom($modulePath. 'Migrations');
                    $this->loadTranslationsFrom($modulePath.'Translations', $moduleName);

                    if ($this->app->runningInConsole()) {
                        $moduleCommands = [];
                        $commandDirPath = $modulePath. 'Commands';
                        if (File::isDirectory($commandDirPath)) {
                            foreach (File::files($modulePath. 'Commands') as $file) {
                                $pathInfo = pathinfo($file);
                                $moduleCommands[] = "App\\Modules\\" .Str::studly($module). "\\Commands\\{$pathInfo['filename']}";
                            }
                            $this->commands($moduleCommands);
                        }
                    }

                    // register middle wares
                    $moduleMiddleware = config($module. '.middleware', []);
                    foreach ($moduleMiddleware as $key => $middleware) {
                        $this->app->get('router')->aliasMiddleware($key , $middleware);
                    }

                    // register service provider
                    $moduleProviders = config($module. '.providers', []);
                    foreach ($moduleProviders as $provider) {
                        $this->app->register($provider);
                    }
                }
            }
        }
    }

    /**
     *
     * Enable i18n
     */
    private function enableI18n(): self
    {
        if( config($this->config. '.i18n') )
        {
            $requested_lang = (strlen(request()->segment(1)) === 2) ? request()->segment(1) : '';
            $requested_country = (strlen(request()->segment(2)) === 2) ? request()->segment(2) : '';

            // set the local for application
            // very important to support multi-languages
            app()->setLocale($requested_lang ?: config('app.fallback_locale'));

            // set url prefix for routing purpose
            config(['langPrefix' => trim("{$requested_lang}/{$requested_country}", DIRECTORY_SEPARATOR)]);
        }
        return $this;
    }
}
